
# mount volumes
zpool import -d /dev/nvme0n1p2 -R /mnt zroot
mount /dev/nvme0n1p1 /mnt/boot

# chroot and fix
arch-chroot /mnt /bin/bash
pacman -Sy
pacman -S linux

mkinitcpio -p linux